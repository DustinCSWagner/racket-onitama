#lang racket

(require racket/enter 2htdp/image 2htdp/universe lang/posn)

; todo: ability to pass turn when no move is available
; todo: cleaner organization ;(enter! "serve.rkt") or requrie / provide
;     https://docs.racket-lang.org/reference/interactive.html#%28form._%28%28lib._racket%2Fenter..rkt%29._enter%21%29%29
;     https://docs.racket-lang.org/guide/module-basics.html
; todo: thinking overlay while intelligent agent runs

;; structures ------------------------------------------------------------------
;a game object is initialized from a full deck with 5 random cards selected,
;    player info, and 10 pieces (4 red pawns 1 red king, 4 blue pawns 1 blue king)
;player1 selects one of her two cards to apply to one of her pieces.
; the piece moves and creates a new board state (possibly removing a piece),
; the card is then exchanged for the trade card, and 
; the board is checked for game-over; if its not over then a new game state is
;     created with player2 as the current player

;used in card-grids to indicate relative movement
; and game board to indicate piece locations
;top left is (1,1) top right is (5,1)
;middle is (3,3) bottom right is (5,5)
(struct board-posn (x y) #:transparent)

; a piece has name, a board-posn, an owner (player 1 or 2), and an image
(struct piece (name posn owner image) #:transparent)

;name is a string
;movement is a list of board-posns possible movement destinations for a piece
; the central spot (3,3) represents a piece to be moved and is
; excluded from the movement list
;card-slot used to indicate one of the 5 available slots
; 1 and 2 belong to player 1
; 3rd slot location is the swap card between players
; 4 and 5 belong to player 2
;starting-player is 1 or 2
(struct card (name movement slot starting-player) #:transparent)

;the state of the game, the world-state, or world
;a list of pieces (with their positions)
;a list of cards (with their card-slot)
;the current player (player 1 or 2), their turn's actions will change the game's state 
;the current player's selected piece (piece)
;the current player's selected card (card)
;the current palyer's selected destination for the piece & card (board-posn)
(struct state (pieces
               cards
               current-player
               selected-piece
               selected-card
               selected-dest) #:transparent)

;a move consists of a selected piece, destination, and card
(struct move (piece
              dest
              card) #:transparent)

;; graphics --------------------------------------------------------------------
; Game play settings
(define BOARD-WIDTH 5)
(define BOARD-HEIGHT 5)
(define HAND-SIZE 2)
; Graphics
(define WIDTH-PX 1200)
(define HEIGHT-PX 600)
(define TICK-RATE 1/10)
;; Visual constants
(define MT-SCENE (empty-scene WIDTH-PX (add1 HEIGHT-PX) "Linen"))
(define SCALE (/ WIDTH-PX 10))
(define HALF-SCALE (/ SCALE 2))
(define QTR-SCALE (/ SCALE 4))
(define GRID-SIZE (* SCALE 5))
(define RED-PAWN (underlay (radial-star 24
                                        (- HALF-SCALE 15)
                                        (- HALF-SCALE 10)
                                        "solid"
                                        "tomato")
                          (radial-star 24
                                       (- HALF-SCALE 25)
                                       (- HALF-SCALE 20)
                                       "solid"
                                       "red")))
(define RED-KING (underlay RED-PAWN
                          (star QTR-SCALE "solid" "white")))
(define BLUE-PAWN (underlay (radial-star 24
                                         (- HALF-SCALE 15)
                                         (- HALF-SCALE 10)
                                         "solid"
                                         "royalblue")
                          (radial-star 24
                                       (- HALF-SCALE 25)
                                       (- HALF-SCALE 20)
                                       "solid"
                                       "blue")))
(define BLUE-KING (underlay BLUE-PAWN
                          (star QTR-SCALE "solid" "white")))

(define CELL (square SCALE "outline" "black"))
(define ROW (place-images (list
                           CELL
                           CELL
                           CELL
                           CELL
                           CELL)
                          (list
                           (make-posn HALF-SCALE HALF-SCALE)
                           (make-posn (+ HALF-SCALE SCALE) HALF-SCALE)
                           (make-posn (+ HALF-SCALE (* SCALE 2)) HALF-SCALE)
                           (make-posn (+ HALF-SCALE (* SCALE 3)) HALF-SCALE)
                           (make-posn (+ HALF-SCALE (* SCALE 4)) HALF-SCALE))
                          (rectangle (add1 GRID-SIZE) (add1 SCALE) "outline" "black")))
(define GRID (place-images (list
                            ROW
                            ROW
                            ROW
                            ROW
                            ROW)
                           (list
                            (make-posn (add1 (/ GRID-SIZE 2))
                                       (add1 (+ HALF-SCALE (* SCALE 0))))
                            (make-posn (add1 (/ GRID-SIZE 2))
                                       (add1 (+ HALF-SCALE (* SCALE 1))))
                            (make-posn (add1 (/ GRID-SIZE 2))
                                       (add1 (+ HALF-SCALE (* SCALE 2))))
                            (make-posn (add1 (/ GRID-SIZE 2))
                                       (add1 (+ HALF-SCALE (* SCALE 3))))
                            (make-posn (add1 (/ GRID-SIZE 2))
                                       (add1 (+ HALF-SCALE (* SCALE 4)))))
                           (rectangle (add1 GRID-SIZE)
                                      (add1 GRID-SIZE) "solid" "Linen")))

(define CELL-MARK (square (sub1 SCALE) "solid" "DarkGray"))
(define CELL-HIGHLIGHT (square (sub1 SCALE) "solid" "Goldenrod"))
(define KING-HIGHLIGHT (square SCALE "solid" (color 240 120 0 100)))

(define CLICKED-PIECE (square SCALE "outline" (pen "green" 10 "dot" "projecting" "bevel")))
(define CLICKED-DEST (square SCALE "outline" (pen "yellow" 10 "dot" "projecting" "bevel")))
(define CLICKED-CARD (rectangle 220 180 "outline" (pen "green" 10 "dot" "projecting" "bevel")))


;min-x, max-x, min-y, max-y
(define card-one-bounds (list 700 900 20 180))
(define card-two-bounds (list 950 1150 20 180))
(define card-three-bounds (list 700 900 370 530))
(define card-four-bounds (list 950 1150 370 530))
(define card-five-bounds (list 800 1000 195 355))

;for card highlighting, gives the top-left corner to place the highlight
(define card-position-x (list (first card-one-bounds)
                              (first card-two-bounds)
                              (first card-three-bounds)
                              (first card-four-bounds)
                              (first card-five-bounds)))

(define card-position-y (list (third card-one-bounds)
                              (third card-two-bounds)
                              (third card-three-bounds)
                              (third card-four-bounds)
                              (third card-five-bounds)))

;; game pieces and settings ----------------------------------------------------
;some cards
(define tiger-card (card "Tiger"
                         (list (board-posn 3 1)
                               (board-posn 3 4))
                         0
                         1))
(define goose-card (card "Goose"
                         (list (board-posn 2 2)
                               (board-posn 2 3)
                               (board-posn 4 3)
                               (board-posn 4 4))
                         0
                         2))
(define ox-card (card "Ox"
                      (list (board-posn 3 2)
                            (board-posn 4 3)
                            (board-posn 3 4))
                      0
                      2))
(define dragon-card (card "Dragon"
                         (list (board-posn 1 2)
                               (board-posn 5 3)
                               (board-posn 2 4)
                               (board-posn 4 4))
                         0
                         1))
(define frog-card (card "Frog"
                         (list (board-posn 1 3)
                               (board-posn 2 2)
                               (board-posn 4 4))
                         0
                         1))
(define rabbit-card (card "Rabbit"
                         (list (board-posn 2 4)
                               (board-posn 4 2)
                               (board-posn 5 3))
                         0
                         1))
(define crab-card (card "Crab"
                         (list (board-posn 1 3)
                               (board-posn 3 2)
                               (board-posn 5 3))
                         0
                         2))
(define elephant-card (card "Elephant"
                         (list (board-posn 2 2)
                               (board-posn 2 3)
                               (board-posn 4 2)
                               (board-posn 4 3))
                         0
                         1))
(define rooster-card (card "Rooster"
                         (list (board-posn 2 3)
                               (board-posn 2 4)
                               (board-posn 4 2)
                               (board-posn 4 3))
                         0
                         1))
(define monkey-card (card "Monkey"
                         (list (board-posn 2 2)
                               (board-posn 2 4)
                               (board-posn 4 2)
                               (board-posn 4 4))
                         0
                         2))
(define mantis-card (card "Mantis"
                         (list (board-posn 2 2)
                               (board-posn 4 2)
                               (board-posn 3 4))
                         0
                         1))
(define horse-card (card "Horse"
                         (list (board-posn 2 3)
                               (board-posn 3 2)
                               (board-posn 3 4))
                         0
                         1))
(define crane-card (card "Crane"
                         (list (board-posn 3 2)
                               (board-posn 2 4)
                               (board-posn 4 4))
                         0
                         2))
(define boar-card (card "Boar"
                         (list (board-posn 3 2)
                               (board-posn 2 3)
                               (board-posn 4 3))
                         0
                         1))
(define eel-card (card "Eel"
                         (list (board-posn 2 2)
                               (board-posn 4 3)
                               (board-posn 2 4))
                         0
                         2))
(define cobra-card (card "Cobra"
                         (list (board-posn 2 3)
                               (board-posn 4 2)
                               (board-posn 4 4))
                         0
                         1))
(define kraken-card (card "Kraken"
                         (list (board-posn 1 1)
                               (board-posn 5 5)
                               (board-posn 1 5)
                               (board-posn 5 1)
                               (board-posn 2 2)
                               (board-posn 4 2)
                               (board-posn 2 4)
                               (board-posn 4 4))
                         0
                         2))
(define phoenix-card (card "Phoenix"
                         (list (board-posn 1 3)
                               (board-posn 2 2)
                               (board-posn 4 2)
                               (board-posn 5 3))
                         0
                         2))
(define turtle-card (card "Turtle"
                         (list (board-posn 1 3)
                               (board-posn 2 4)
                               (board-posn 4 4)
                               (board-posn 5 3))
                         0
                         1))


;test deck
(define TEST-DECK (shuffle (list tiger-card
                            tiger-card
                            goose-card
                            goose-card
                            ox-card
                            ox-card)))
(define DECK (list tiger-card 
                   goose-card
                   ox-card 
                   dragon-card 
                   frog-card
                   crab-card 
                   elephant-card 
                   rooster-card
                   monkey-card
                   mantis-card 
                   horse-card 
                   crane-card 
                   boar-card 
                   eel-card 
                   cobra-card 
                   kraken-card
                   phoenix-card
                   turtle-card))

(define (initial-pieces)
  (list (piece "rp1" (board-posn 1 1) 1 RED-PAWN)
        (piece "rp2" (board-posn 2 1) 1 RED-PAWN)
        (piece "rk" (board-posn 3 1) 1 RED-KING)
        (piece "rp3" (board-posn 4 1) 1 RED-PAWN)
        (piece "rp4" (board-posn 5 1) 1 RED-PAWN)
        (piece "bp1" (board-posn 1 5) 2 BLUE-PAWN)
        (piece "bp2" (board-posn 2 5) 2 BLUE-PAWN)
        (piece "bk" (board-posn 3 5) 2 BLUE-KING)
        (piece "bp3" (board-posn 4 5) 2 BLUE-PAWN)
        (piece "bp4" (board-posn 5 5) 2 BLUE-PAWN)))

;for a list of cards, shuffle and return 5 cards
(define (deal-cards deck)
  (take (shuffle deck) 5))

;for the dealt cards, assign their initial card slot
(define (set-card-slots hand)
  (map card (map card-name hand)
       (map card-movement hand)
       (map + (map card-slot hand) (range 1 (add1 5) 1))
       (map card-starting-player hand)))

(define no-piece (piece "no-piece"
                        (board-posn -1 -1)
                        0
                        (triangle 40 "solid" "black")))

(define no-card (card "no-card"
                      (board-posn -1 -1)
                      0
                      1))

(define no-dest (board-posn 0 0))

(define initial-5-cards (set-card-slots (deal-cards DECK)))

(define initial-world (state
                       (initial-pieces)
                       initial-5-cards
                       (card-starting-player (last initial-5-cards))
                       no-piece
                       no-card
                       no-dest))

;; helper functions ------------------------------------------------------------
;; board-posn Posn -> Boolean
;; Are the two posns are equal?
;; > (posn=? (posn 1 1) (posn 1 1))
;; true
(define (board-posn=? p1 p2)
  (and (= (board-posn-x p1) (board-posn-x p2))
       (= (board-posn-y p1) (board-posn-y p2))))

;remove something in a list at a given position
; returns list with element removed
; if out of bounds, returns original list
(define (remove-at lst pos)
    (cond
      ; position greater/eq than zero, less/eq length-1
      [(and (>= pos 0) (<= pos (- (length lst) 1)))
        (cond
          ; to remove first element
          [(= pos 0) (drop lst 1)]
          ; to remove last element
          [(= pos (- (length lst) 1)) (drop-right lst 1)]
          ; otherwise
          [else (append (take lst pos)
                        (drop lst (+ pos 1)))])]
      ; otherwise return lst
       [lst]))

; return an element from list at a position
; if out of bounds, return empty list
(define (take-at lst pos)
  (cond
    [(and (>= pos 0) (<= pos (sub1 (length lst))))
     (first (drop lst pos))]
    [else '()]))

;find the game piece by its name
; if the piece is not in the list of pieces in the current
; state, then return no-piece
(define (find-piece w name)
  ; index-of will return #f if there is nothing found
  (define i (index-of (map eq? (make-list (length (state-pieces w)) name)
                                        (map piece-name (state-pieces w)))
                                        #t))
  (cond
    [(integer? i) (take-at (state-pieces w) i)]
    [else no-piece]))


; search all game peices in world and return the piece which has the board-posn
; of the location clicked, or return no-piece
(define (get-game-piece w x y)
  ;the board position in grid coord
  (define gx (board-posn (px-to-grid x) (px-to-grid y)))

  ; index-of will return #f if there is nothing found
  (define i (index-of (map board-posn=? (make-list (length (state-pieces w)) gx)
                                        (map piece-posn (state-pieces w)))
                                        #t))
  (cond
    ; check if there is a piece
    ; otherwise return a 'no-piece' piece
    [(integer? i) (last (take (state-pieces w) (add1 i)))]
    [else no-piece]))

; 
(define (get-card-in-slot w slot)
  ; i, the index of selected piece or #f if there is nothing found
  (define i (index-of (map eq? (make-list (length (state-cards w)) slot)
                                        (map card-slot (state-cards w)))
                                        #t))
  (take-at (state-cards w) i))

; for a click in the card area, retun card 1,2,3, or 4
; otherwise no-card
(define (get-card w x y)
  (cond
    ; card-slot 1 bounds
    [(and (>= x (list-ref card-one-bounds 0))
          (<= x (list-ref card-one-bounds 1))
          (>= y (list-ref card-one-bounds 2))
          (<= y (list-ref card-one-bounds 3)))
     ;return card in slot 1
     (get-card-in-slot w 1)]
    [(and (>= x (list-ref card-two-bounds 0))
          (<= x (list-ref card-two-bounds 1))
          (>= y (list-ref card-two-bounds 2))
          (<= y (list-ref card-two-bounds 3)))
     ;return card in slot 2
     (get-card-in-slot w 2)]
    [(and (>= x (list-ref card-three-bounds 0))
          (<= x (list-ref card-three-bounds 1))
          (>= y (list-ref card-three-bounds 2))
          (<= y (list-ref card-three-bounds 3)))
     ;return card in slot 3
     (get-card-in-slot w 3)]
    [(and (>= x (list-ref card-four-bounds 0))
          (<= x (list-ref card-four-bounds 1))
          (>= y (list-ref card-four-bounds 2))
          (<= y (list-ref card-four-bounds 3)))
     ;return card in slot 4
     (get-card-in-slot w 4)]
     [else no-card]))

; returns 1 (red) for a card in slot 1 and 2
; returns 2 (blue) for a card in slot 3 or 4
; defualt is to return player 1
(define (current-card-holder card)
  (cond
    [(or (eq? (card-slot card) 1)
         (eq? (card-slot card) 2))
     1]
    [(or (eq? (card-slot card) 3)
         (eq? (card-slot card) 4))
     2]
    [else 1]))

;takes a click in the board region and returns a board position
(define (px-to-grid px-p)
  (cond
    [(<= px-p HEIGHT-PX)
     (ceiling (/ px-p SCALE))]
    [else 0]))

; for centering images over a grid location
; takes a x or y grid position and will convert to number of pixels
; (1,1) | (1,2) | ... | (1,5)
; (2,1) | (2,2) | ... | (2,5)
;  ...     ...    ... |  ...
; (5,1) | (5,2) | ... | (5,5)
(define (grid-to-px grid-pos)
  (- (* SCALE grid-pos) HALF-SCALE))

; returns a list of the current-players pieces
; or an empty list if player is not one or two
(define (current-player-pieces w)
  (cond
    [(eq? (state-current-player w) 1)
     (filter player-one-piece? (state-pieces w))]
    [(eq? (state-current-player w) 2)
     (filter player-two-piece? (state-pieces w))]
    [else '()]))

;return the opposite of void?
(define (not-void? x)
  (not (void? x)))


; triggered on a confirmation click
; given the world state, return an updated list of pieces with the pieces moved
; if a piece is moving onto another piece, remove it
(define (move-pieces w)
  ; find the piece that was selected and update its board-posn to the selected destination
  ; i, the index of selected piece or #f if there is nothing found
  (define i (index-of (map eq? (make-list (length (state-pieces w)) (piece-name (state-selected-piece w)))
                                        (map piece-name (state-pieces w)))
                                        #t))
  
  ; find if there is a piece at the selected-dest and remove it if it exists
  ; j, the index of the piece to be captured or #f if there is nothing found
  (define j (index-of (map board-posn=? (make-list (length (state-pieces w)) (state-selected-dest w))
                                        (map piece-posn (state-pieces w)))
                                        #t))
  (cond
    ; check if there is a piece to move
    [(integer? i)
     ; if there is a piece that matches, is there a piece to remove
     (cond
       [(integer? j)
        ; return a list of pieces with i updated and j dropped
        (remove-at (list-set (state-pieces w)
                  i
                  (piece (piece-name (state-selected-piece w))
                         (state-selected-dest w)
                         (piece-owner (state-selected-piece w))
                         (piece-image (state-selected-piece w))))
              j)]
       ; otherwise return a list of pieces with i updated
       [else (list-set (state-pieces w)
                  i
                  (piece (piece-name (state-selected-piece w))
                         (state-selected-dest w)
                         (piece-owner (state-selected-piece w))
                         (piece-image (state-selected-piece w))))])]
    ; if there is no selected piece somehow, return a 'no-piece' piece
    [else no-piece]))

; takes current world and retuns the next player (1 or 2)
(define (update-player w)
  (cond
    [(eq? 1 (state-current-player w))
     2]
    [else 1]))

; exchange selected card with swap card in slot 5
; return the updated list of 5 cards in play
(define (move-cards w)
  ; find the position of the swap card
  (define i (index-of (map eq? (make-list (length (state-cards w)) 5)
                                        (map card-slot (state-cards w)))
                                        #t))
  ; find the position of the selected card
  (define j (index-of (map eq? (make-list (length (state-cards w)) (card-slot (state-selected-card w)))
                                        (map card-slot (state-cards w)))
                                        #t))
  ; set swap card as selected card-slot
  ; set selected card as swap card slot, 5
  ; (state-cards w)
  ; test i and j
  (list-set
   (list-set (state-cards w) i (card (card-name (take-at (state-cards w) i))
                                     (card-movement (take-at (state-cards w) i))
                                     (card-slot (state-selected-card w))
                                     (card-starting-player (take-at (state-cards w) i))))
   j
   (card (card-name (take-at (state-cards w) j))
         (card-movement (take-at (state-cards w) j))
         5
         (card-starting-player (take-at (state-cards w) j)))))
  
; returns #t if get-winner is player 1 or 2
; otherwise #f
(define (winner? w)
  (cond
    [(or (eq? 1 (get-winner w))
         (eq? 2 (get-winner w)))
     #t]
    [else #f]))

;determines if win conditons are present
; there is a winner if one of the kings have been removed
; there is a winner if one of the kings is in the other kings starting location
;  "rk" (board-posn 3 1) red king starts at 3-1
;  "bk" (board-posn 3 5) blue kind starts at 3-5
(define (get-winner w)
  (cond
    ;if blue king is in 3,1 then blue wins
    [(eq? "bk" (piece-name
                (get-game-piece w (grid-to-px 3) (grid-to-px 1))))
     2]
    ;if red king is in 3,5 then red wins
    [(eq? "rk" (piece-name
                (get-game-piece w (grid-to-px 3) (grid-to-px 5))))
     1]
    ;if blue is eliminated then red wins
    [(eq? "no-piece" (piece-name (find-piece w "bk")))
     1]
    ;if red is eliminated then blue wins
    [(eq? "no-piece" (piece-name (find-piece w "rk")))
     2]))

; returns a single card object from the world for a given card slot
(define (card-in-slot w s)
  (list-ref (state-cards w)
            (index-of (map card-slot (state-cards w)) s)))

;; intelligent agent functions -------------------------------------------------
; given a state, return a list of all valid moves (piece dest card)
(define (all-moves w)
  ;for each of the current-player's pieces
    ;apply each card to the pieces
      ;check each possible destination in a piece-card combo
      ;check if the move is valid
      ;if valid, add the move to the list of moves
  (filter not-void? (flatten
                     (for/list ([piece (current-player-pieces w)]
                               #:when (not-void? piece)
                               [card (current-player-cards w)])
                               (for/list
                                   ([dest (possible-destinations w piece card)])
                                 (cond
                                   [(valid-move? w (move piece dest card))
                                    (move piece dest card)]))))))

;retruns #t if piece owner == 1
(define (player-one-piece? piece)
  (eq? (piece-owner piece) 1))

;retruns #t if piece owner == 2
(define (player-two-piece? piece)
  (eq? (piece-owner piece) 2))

; returns a list of the current-players cards
; or an empty list if player is not one or two
(define (current-player-cards w)
  (cond
    [(eq? (state-current-player w) 1)
     (list (get-card-in-slot w 1) (get-card-in-slot w 2))]
    [(eq? (state-current-player w) 2)
     (list (get-card-in-slot w 3) (get-card-in-slot w 4))]
    [else '()]))

;naively returns a list of all possible destinatios for a given card and piece
; the x and y modifiers need to be negative if they are greater than 3
; eg 3,3 is piece origin
;   4,4 is current position +1,+1
;   3,2 is current position +0,-1
; world is needed to know current player
;   player 1's cards are rotated 180deg
;   xs that are neg (to the left) need to be positive
;   ys that are neg (down) need to be positive
(define (possible-destinations w piece card)
 (cond
   ;normal orientation
   [(eq? 2 (state-current-player w))
    (map (lambda (x y)
           (board-posn (+ (board-posn-x (piece-posn piece)) x)
                       (+ (board-posn-y (piece-posn piece)) y)))
         (map (lambda (x) (- x 3)) (map board-posn-x (card-movement card)))
         (map (lambda (y) (- y 3)) (map board-posn-y (card-movement card))))]
   ;reverse orientation
   [(eq? 1 (state-current-player w))
    (map (lambda (x y)
           (board-posn (+ (board-posn-x (piece-posn piece)) x)
                       (+ (board-posn-y (piece-posn piece)) y)))
         (map (lambda (x) (* (- x 3) -1)) (map board-posn-x (card-movement card)))
         (map (lambda (y) (* (- y 3) -1)) (map board-posn-y (card-movement card))))]))

;create a new world from a word and move
(define (new-world w move)
  (define w1 (state (state-pieces w)
                    (state-cards w)
                    (state-current-player w)
                    (move-piece move)
                    (move-card move)
                    (move-dest move)))
  (state (move-pieces w1)
         (move-cards w1)
         (update-player w1)
         no-piece
         no-card
         no-dest))

;random opponent returns a random valid move
(define (random-move w)
   (first (shuffle (all-moves w))))

;random walk to depth n
(define (random-walk w n)
  (define m (random-move w))
  (define w1 (new-world w m))
  (cond
    ;n=0 -> score current world
    [(= n 0)
     (* -1 (score-world w))]
    ;move causes end-game-world -> score end-game-world
    [(winner? w1)
     (cond
       [(odd? n)
       (* 1 (score-world w1))]
       [(even? n)
       (* -1 (score-world w1))])]
    ;otherwise, evaluate random move and random walk further
    [else
     (cond
       [(odd? n)
        (+ (* 1 (score-world w1)) (random-walk w1 (sub1 n)))]
       [(even? n)
        (+ (* -1 (score-world w1)) (random-walk w1 (sub1 n)))])]))

;applies random walk to all of initial world's moves to depth n
;returns move with highest score
(define (choose-walk w n)
  (define scores (map random-walk
                      (make-list (length (all-moves w)) w)
                      (make-list (length (all-moves w)) n)))
  (define best-score (apply max scores))
  (define move-index (index-of scores best-score))
  (take-at (all-moves w) move-index))

;applies random walk to all of initial world's moves to depth n
;reruns to that depth k times
;returns move with highest total score
(define (multi-walk w n k)
   (cond
    [(= k 0)
     (random-walk w n)]
    [else
     (+ (random-walk w n) (multi-walk w n (sub1 k)))]))

;multiwalk
(define (choose-multi-walk w n k)
  (define scores (map multi-walk
                      (map new-world
                           (make-list (length (all-moves w)) w)
                           (all-moves w))
                      (make-list (length (all-moves w)) n)
                      (make-list (length (all-moves w)) k)))
  (define best-score (apply max scores))
  (define move-index (index-of scores best-score))
  (take-at (all-moves w) move-index))

(define (choose-minimax w n)
  (define minimax-scores (minimax-moves w n))
  (define best-score (apply max minimax-scores))
  (define move-index (index-of minimax-scores best-score))
  (take-at (all-moves w) move-index))

;returns a score for each possible move from initial world
(define (minimax-moves w n)
  (map minimax (map new-world
                    (make-list (length (all-moves w)) w)
                    (all-moves w))
               (make-list (length (all-moves w)) #t)
               (make-list (length (all-moves w)) n)))

;score the moves using minimax
;returns a list of scores for each possible move
(define (minimax w max? n)
  ;level-moves are all possible moves for this world
  (define level-moves (all-moves w))
  (cond
    ;positive if maximizing
    [max?
     (cond
       [(or (= n 0) (winner? w))
        ;score a random world ~ maybe faster
        (score-move w (random-move w))]
        ;if n=0 score all the worlds for all possible moves (a modification on minimax)
        ;(foldl + 1 (map score-move
        ;                (make-list (length level-moves) w)
        ;                level-moves))]
       [else 
        ;otherwise evaluate each new world
        (foldl + 0 (map minimax (map new-world
                                            (make-list (length level-moves) w)
                                            level-moves)
                        (make-list (length level-moves) (not max?))
                        (make-list (length level-moves) (sub1 n))))])]
    ;negative scores
     [else
      (cond
        [(or (= n 0) (winner? w))
         (* -1 (score-move w (random-move w)))]
         ;if n=0 or score the world for all possible moves (a modification on minimax)
         ;(* -1 (foldl + 0 (map score-move
         ;                (make-list (length level-moves) w)
         ;                level-moves)))]
        [else 
         ;otherwise evaluate each new world
         (foldl + 0 (map minimax (map new-world
                                            (make-list (length level-moves) w)
                                            level-moves)
                               (make-list (length level-moves) (not max?))
                               (make-list (length level-moves) (sub1 n))))])]))
 

;for an initial-world (w0) and a given move (piece dest card)
; returns #t if the move is valid of #f if not
;a vaild move:
; * uses a valid card for the current player,
; * uses a valid piece for the current player,
; * stays on the board,
; * doesn't attempt to capture your own piece
; * reachable destination for a given piece and card
(define (valid-move? w0 move)
  (define w1 (state (state-pieces w0)
                    (state-cards w0)
                    (state-current-player w0)
                    (move-piece move)
                    (move-card move)
                    (move-dest move)))
  (cond
    [(and (valid-card? w0 move)
          (valid-piece? w0 move)
          (on-board? (move-dest move))
          (no-self-capture? w0 move)
          (reachable-dest? w0 move))
     #t]
    [else #f]))

; returns true if current player is using a one of their cards
(define (valid-card? w move)
  (cond
    ;player 1 uses card slots 1 and 2
    [(and (eq? 1 (state-current-player w))
          (or (eq? 1 (card-slot (move-card move)))
              (eq? 2 (card-slot (move-card move)))))
     #t]
    ;player 2 uses card slots 3 and 4
    [(and (eq? 2 (state-current-player w))
          (or (eq? 3 (card-slot (move-card move)))
              (eq? 4 (card-slot (move-card move)))))
     #t]
    [else #f]))

; returns true if current player is using a one of their pieces
(define (valid-piece? w move)
  (cond
    ;player 1 uses pieces owned by 1
    [(and (eq? 1 (state-current-player w))
          (eq? 1 (piece-owner (move-piece move))))
     #t]
    ;player 2 uses pieces owned by 2
    [(and (eq? 2 (state-current-player w))
          (eq? 2 (piece-owner (move-piece move))))
     #t]
    [else #f]))

    
; returns #t if board-posn is in the 1x5 by 1x5 grid
(define (on-board? posn)
  (and (>= (board-posn-x posn) 1)
       (<= (board-posn-x posn) 5)
       (>= (board-posn-y posn) 1)
       (<= (board-posn-y posn) 5)))

;given a state and a move, would a move capture a piece that you own?
(define (no-self-capture? w move)
  (cond
    ;player 1 is moving and has selected a destination with one of their pieces -> #f
    [(and (eq? 1 (state-current-player w))
          (eq? 1 (piece-owner (get-game-piece w
                                              (grid-to-px (board-posn-x (move-dest move)))
                                              (grid-to-px (board-posn-y (move-dest move)))))))
     #f]
    ;player 2 is moving and has selected a destination with one of their pieces -> #f
    [(and (eq? 2 (state-current-player w))
          (eq? 2 (piece-owner (get-game-piece w
                                              (grid-to-px (board-posn-x (move-dest move)))
                                              (grid-to-px (board-posn-y (move-dest move)))))))
     #f]
    [else #t]))

;given a world and move, can the selected piece move to that destination using the selected card?
(define (reachable-dest? w0 move)
  (define p-dest (possible-destinations  w0 (move-piece move) (move-card move)))
  (if (< 0 (length p-dest))
      (if (index-of p-dest (move-dest move)) #t #f)
       #f))
      
;takes a list of moves and evaluates the n resulting worlds taking the max/min of each world-level
;for n=2
;w0 -> player move -> w1 -> all valid opponent moves -> w2
;evaluates all opponent moves and returns minimum


;for given move and world, evaluate the score for the current player
(define (score-move w move)
  ;w0 with given move
  (define w0 (state (state-pieces w)
                    (state-cards w)
                    (state-current-player w)
                    (move-piece move)
                    (move-card move)
                    (move-dest move)))
  ;w1, resulting world
  (define w1 (state (move-pieces w0)
                    (move-cards w0)
                    (update-player w0)
                    no-piece
                    no-card
                    no-dest))
  
  (cond
    ;was the game won?
    [(winner? w1) 10]
    ;was a non-king piece captured? +5
    [(and (< (length (state-pieces w0)) (length (state-pieces w1)))
          (opponent-king-present? w1))
     5]   
    ;otherwise 0
    [else 0]))

;evaluate a world
(define (score-world w)
  (cond
    [(winner? w) 10]
    [else 0]))

; given a resultant world, check if the opponent's (the world's current-player) king is present
(define (opponent-king-present? w)
  (define current-player (update-player w))
  (cond
    [(eq? current-player 1)
     ;player 1 is red, does a search of bk return no-piece
     ;if it does, then the opponent king is not present, reverse to match function name
     (not (eq? "no-piece" (piece-name (find-piece w "bk"))))]
    ; otherwise player 2
    ; reverse of 'is rk absent?'
    [else (not (eq? "no-piece" (piece-name (find-piece w "rk"))))]))

;; rendering  ------------------------------------------------------------------
;; this creates the grid to be used on the playing cards
;; the cell-mark square represents the location of a player's piece
;; the cell-highlights represent all possible movement locations relative to
;;    the selected piece location
(define (render-card-grid card)
  (place-images
   (append (list CELL-MARK)
         (make-list (length (card-movement card)) CELL-HIGHLIGHT))
   (append (list (make-posn (grid-to-px 3)
                    (grid-to-px 3)))
           (map make-posn
                (map grid-to-px
                     (map board-posn-x
                          (card-movement card)))
                (map grid-to-px
                     (map board-posn-y
                          (card-movement card)))))
   GRID))

;returns "red" for player 1 and "blue" for player 2
(define (card-color card)
  (cond
    [(eq? 1 (card-starting-player card))
     "red"]
    [else "blue"]))

;; this renders a card image including text and grid with center
;; square highlight and relative movement squares
(define (render-card card)
  (place-images
   (list (rotate 90 (text (card-name card) 24 "black"))
         (scale 1/5 (render-card-grid card))
         (rectangle 20 20 "solid" (card-color card)))
   (list (make-posn 30 80)
         (make-posn 120 80)
         (make-posn 180 140))
   (empty-scene 200 160 "brown")))

;renders the 5x5 board and pieces
;and king-kighlights
(define (render-board w)
   (place-images
    (map piece-image (state-pieces w))
    (map make-posn
         (map grid-to-px
              (map board-posn-x
                   (map piece-posn (state-pieces w))))
         (map grid-to-px
              (map board-posn-y
                   (map piece-posn (state-pieces w)))))
    (underlay/xy
     (underlay/xy
        GRID
        (+ (* 2 SCALE) 1) 1 KING-HIGHLIGHT)
      (+ (* 2 SCALE) 1) (+ (* 4 SCALE) 1) KING-HIGHLIGHT)))

;redners the entire play area
(define (render-table w)
  (underlay/xy
   (underlay/xy
    (underlay/xy
     (place-images
      (list (render-board w)
            (rotate 180 (render-card (card-in-slot w 1)))
            (rotate 180 (render-card (card-in-slot w 2)))
            (render-card (card-in-slot w 3))
            (render-card (card-in-slot w 4))
            (render-card (card-in-slot w 5))
            (turn-indicator w)
            (confirm-button w))
      (list (make-posn 300 300)
            (make-posn 800 100)
            (make-posn 1050 100)
            (make-posn 800 450)
            (make-posn 1050 450)
            (make-posn 900 275)
            (make-posn 1175 300)
            (make-posn 900 565))
      MT-SCENE)
     (card-highlight-x w) (card-highlight-y w)
     (card-highlight w))
    (dest-highlight-x w) (dest-highlight-y w)
    (dest-highlight w))
   (source-highlight-x w) (source-highlight-y w)
   (source-highlight w)))

; confirm button greyed-out until player has selected
;  a piece to move, a destination, and a card
; then the confirm button is green
(define (confirm-button w)
  (cond
    [(and (not (eq? "no-piece" (piece-name (state-selected-piece w))))
          (not (eq? "no-card" (card-name (state-selected-card w))))
          (not (eq? (board-posn-x (state-selected-dest w)) 0))
          (valid-move? w (move (state-selected-piece w) (state-selected-dest w) (state-selected-card w))))
               (underlay (rectangle 500 50 "solid" "green")
                         (rectangle 498 48 "outline" "black")
                         (text "Confirm" 20 "black"))]
    [else (underlay (rectangle 500 50 "solid" "grey")
                         (rectangle 498 48 "outline" "black")
                         (text "Confirm" 20 "black"))]))
     
; returns nothing or the highlight if one has been selected
(define (dest-highlight w)
  (cond
    [(not (eq? (board-posn-x (state-selected-dest w)) 0))
        CLICKED-DEST]
    [else (square 0 "outline" "black")]))

; returns the make-posn of the destination highlight if
; a destination has been selected
(define (dest-highlight-x w)
  (cond
    [(not (eq? (board-posn-x (state-selected-dest w)) 0))
     (- (grid-to-px (board-posn-x (state-selected-dest w))) HALF-SCALE)]
    [else 0]))

(define (dest-highlight-y w)
  (cond
    [(not (eq? (board-posn-y (state-selected-dest w)) 0))
     (- (grid-to-px (board-posn-y (state-selected-dest w))) HALF-SCALE)]
    [else 0]))

; returns nothing or the highlight if one has been selected
(define (source-highlight w)
  (cond
    [(not (eq? "no-piece" (piece-name (state-selected-piece w))))
        CLICKED-PIECE]
    [else (square 0 "outline" "black")]))

(define (source-highlight-x w)
  (cond
    [(not (eq? "no-piece" (piece-name (state-selected-piece w))))
     (- (grid-to-px (board-posn-x
                     (piece-posn (state-selected-piece w))))
        HALF-SCALE)]
    [else 0]))

(define (source-highlight-y w)
  (cond
    [(not (eq? "no-piece" (piece-name (state-selected-piece w))))
     (- (grid-to-px (board-posn-y
                     (piece-posn (state-selected-piece w))))
        HALF-SCALE)]
    [else 0]))

; returns nothing or the highlight if one has been selected
(define (card-highlight w)
  (cond
    [(not (eq? "no-card" (card-name (state-selected-card w))))
        CLICKED-CARD]
    [else (square 0 "outline" "black")]))

(define (card-highlight-x w)
  (cond
    [(not (eq? "no-card" (card-name (state-selected-card w))))
     (- (take-at card-position-x (sub1 (card-slot (state-selected-card w)))) 10)]
    [else 0]))

(define (card-highlight-y w)
  (cond
    [(not (eq? "no-card" (card-name (state-selected-card w))))
     (- (take-at card-position-y (sub1 (card-slot (state-selected-card w)))) 10)]
    [else 0]))

; creates a 10x500 indicator bar
; red/player 1 is on top
; blue/player 2 is on bottom
(define (turn-indicator w)
  (cond
    [(eq? 1 (state-current-player w))
     (place-images (list (rectangle 10 250 "solid" "red"))
              (list (make-posn 5 125))
              (empty-scene 10 500 "grey"))]
    [else (place-images (list (rectangle 10 250 "solid" "blue"))
              (list (make-posn 5 375))
              (empty-scene 10 500 "grey"))]))

; used at end of game
(define (render-winner w)
  (cond
    [(eq? 1 (get-winner w))
     (underlay
      (render-table w)
      (rectangle WIDTH-PX HEIGHT-PX "solid" (color 200 0 0 100))
      (text/font "Red Wins!" 120
            "white" "Gill Sans" 'swiss 'normal 'bold #f))]
    [(eq? 2 (get-winner w))
     (underlay
      (render-table w)
      (rectangle WIDTH-PX HEIGHT-PX "solid" (color 0 0 200 100))
      (text/font "Blue Wins!" 120
            "white" "Gill Sans" 'swiss 'normal 'bold #f))]))

;used while searching for a move
(define (render-thinking w)
  (underlay
      (render-table w)
      (rectangle WIDTH-PX HEIGHT-PX "solid" (color 0 0 0 100))
      (text/font "Thinking ... " 60
            "white" "Gill Sans" 'swiss 'normal 'bold #f)))

;; big-bang and input ----------------------------------------------------------
;; yay
;https://docs.racket-lang.org/teachpack/2htdpuniverse.html?q=big-bang
(define (start-game)
  (big-bang initial-world
    ;(on-tick check-thinking 0.02)
    (on-key check-agent)
    (on-mouse handle-mouse) 
    (to-draw render-table)
    (stop-when winner? render-winner)
    (name "Onitama")
    (display-mode 'normal))) ;'normal or 'fullscreen

(define thinking #f)
;if thinking then render thinking
(define (check-thinking w)
  (cond
    [thinking
     ;set "thinking overlay"
     (render-thinking w)
     w]
    [else w]))

(define (toggle-think)
  (set! thinking (not thinking)))

;on key make intelligent agent move
(define (check-agent w key)
  (define k-samples 100)
  (define selected-move '())
  (toggle-think)
  (cond
    [(key=? key "f1")
     ;minimax to the w-th level
     (set! selected-move (choose-multi-walk w 1 (* 1 k-samples)))
     (new-world w selected-move)]
    [(key=? key "f2")
     ;minimax to the w-th level
     (set! selected-move (choose-multi-walk w 2 (* 2 k-samples)))
     (new-world w selected-move)]
    [(key=? key "f3")
     ;minimax to the w-th level
     (set! selected-move (choose-multi-walk w 3 (* 3 k-samples)))
     (new-world w selected-move)]
    [(key=? key "f4")
     ;minimax to the w-th level
     (set! selected-move (choose-multi-walk w 4 (* 4 k-samples)))
     (new-world w selected-move)]
    [(key=? key "f5")
     ;minimax to the w-th level
     (set! selected-move (choose-multi-walk w 5 (* 5 k-samples)))
     (toggle-think)
     (new-world w selected-move)]
    [(key=? key "f6")
     ;minimax to the w-th level
     (set! selected-move (choose-multi-walk w 6 (* 6 k-samples)))
     (new-world w selected-move)]
    [(key=? key "f7")
     ;minimax to the w-th level
     (set! selected-move (choose-multi-walk w 7 (* 7 k-samples)))
     (new-world w selected-move)]
    [(key=? key "f8")
     ;minimax to the w-th level
     (set! selected-move (choose-multi-walk w 8 (* 8 k-samples)))
     (new-world w selected-move)]
    [(key=? key "f9")
     ;minimax to the w-th level
     (set! selected-move (choose-multi-walk w 9 (* 9 k-samples)))
     (new-world w selected-move)]
    [(key=? key "f10")
     ;minimax to the w-th level
     (set! selected-move (choose-multi-walk w 10 (* 10 k-samples)))
     (new-world w selected-move)]
    [(key=? key "f11")
     ;minimax to the w-th level
     (set! selected-move (choose-multi-walk w 11 (* 11 k-samples)))
     (new-world w selected-move)]
    [(key=? key "f12")
     ;minimax to the w-th level
     (set! selected-move (choose-multi-walk w 12 (* 12 k-samples)))
     (new-world w selected-move)]
    [(or (key=? key "execute") (key=? key "r"))
     ;enter is a rando move
     (set! selected-move (random-move w))
     (new-world w selected-move)]
    ;all other keys status-quo
    [else w]))

(define (handle-mouse w x y e)
  (cond
    [(eq? e "button-down")
     (cond
       ;clicked on the board
       [(and (< x HEIGHT-PX) (positive? x))
        ;clicked on a piece that you own
        ; no-piece is set as the selected-piece after confirm button fires and the turn is processed
        (cond
          [(and
            (not (eq? "no-piece" (piece-name (get-game-piece w x y))))
            (eq? (state-current-player w) (piece-owner (get-game-piece w x y))))
          (state (state-pieces w)
                 (state-cards w)
                 (state-current-player w)
                 (get-game-piece w x y)
                 (state-selected-card w)
                 (state-selected-dest w))]
          ;clicked in the board somewhere so set destination
          [else (state (state-pieces w)
                 (state-cards w)
                 (state-current-player w)
                 (state-selected-piece w)
                 (state-selected-card w)
                 (board-posn (px-to-grid x) (px-to-grid y)))])]
       ;clicked in the card area on one of your cards
       [(and (>= x HEIGHT-PX) (positive? x) (<= y 540)
             (eq? (state-current-player w) (current-card-holder (get-card w x y))))
        (state (state-pieces w)
               (state-cards w)
               (state-current-player w)
               (state-selected-piece w)
               (get-card w x y)
               (state-selected-dest w))]
       ;clicked on the confirm button with a selected piece, card, and dest
       [(and (>= x 650) (<= x 1150) (>= y 540) (<= y 590)
             (not (eq? "no-piece" (piece-name (state-selected-piece w))))
             (not (eq? "no-card" (card-name (state-selected-card w))))
             (not (eq? (board-posn-x (state-selected-dest w)) 0))
             (valid-move? w (move (state-selected-piece w) (state-selected-dest w) (state-selected-card w))))
        ; update state and reset selected pieces
        (state (move-pieces w)
               (move-cards w)
               (update-player w)
               no-piece
               no-card
               no-dest)]
       ;clicked on something - otherwise return current state
       [else w]
       )]
    [w]))

;; start-game ---------------------------------------------------------------------
(start-game)

;; testing ---------------------------------------------------------------------
(define w initial-world)
(define c (first (state-cards w)))
(define p (first (state-pieces w)))

; tests
;(possible-destinations w (first (state-pieces w)) (get-card-in-slot w 1))
;>(list (board-posn 4 3) (board-posn 5 4) (board-posn 4 5)) ; depends on card

; testing intelligent agents
; create world where one move will win
(define tw (state
                       (list (piece "rp1" (board-posn 1 1) 1 RED-PAWN)
                             (piece "rk" (board-posn 3 2) 1 RED-KING)
                             (piece "bp4" (board-posn 5 5) 2 BLUE-PAWN)
                             (piece "bk" (board-posn 3 4) 2 BLUE-KING))
                       (set-card-slots (make-list 5 tiger-card))
                       (card-starting-player tiger-card)
                       no-piece
                       no-card
                       no-dest))

;note: first two moves are equivalent - red pawn move up
;3rd / 5th moves equivalent - red king take blue king
;2nd / 4th equiv - red king move back
;scores for move 3 nd 5 should be highest
;n=1 moves for all others should be zero
;(render-table tw)

;(choose-multi-walk tw 1 1000)

(define tw-socres (map exact->inexact
     (map / (map multi-walk
                 (map new-world
                    (make-list (length (all-moves tw)) tw)
                    (all-moves tw))
                 (make-list (length (all-moves tw)) 1)
                 (make-list (length (all-moves tw)) 1000))
          (make-list (length (all-moves tw)) 1000))))
